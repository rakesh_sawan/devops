﻿$RootDirectory = "Z:\runtime\AWS\AlertSense"
$CFResources = "Z:\runtime\AWS\AlertSense\CFResources"
$SourceDirectory = "Z:\runtime\AWS\AlertSense\scripts"
$OutputDirectory = "Z:\runtime\AWS\AlertSense\output"
$errorPath = $OutputDirectory + "\error.log"

$CloudFormationTemplateSourcePath = $SourceDirectory+"\FullStack.template"
$CloudFormationTemplateOutputPath = $OutputDirectory+"\FullStack.template"

 rm -Recurse $OutputDirectory 
New-Item -Path "$OutputDirectory" -Name "." -ItemType directory

function GetSimpleTypeFromAWSType($awsResourceType){

        $index = $awsResourceType.LastIndexOf("::")
        if($index -gt 0 ){
            $index = $index +2;
        }else{
            $index = 0;
        }
        $awsResourceType = $awsResourceType.Substring($index)
        return $awsResourceType
    
}

function ConvertObjectToJson($ReturnObject){

    $jsonData = $ReturnObject.OutputTemplateObject |ConvertTo-Json -Depth 1000

    $jsonObject = $jsonData |ConvertFrom-Json

    if($jsonObject -eq $null){
        $jsonData = $jsonData  + "}"

    }
    $ReturnObject.JsonData = $jsonData
    
}
function PreProcessDefines($ParameterObject){
    $preProcessors = $ParameterObject.PreProcessors
    $object = $ParameterObject.Object

    echo "Performing PreProcessing $preProcessors"
  
    $objectJsonData = $object|ConvertTo-Json -Depth 1000
   

    foreach($preProcessor in $preProcessors){
        
           echo "Preprocessor $preProcessor"
           $nameValuearray = $preProcessor.ToString().Split("=")
          
           echo "Name:" $nameValueArray[0]
           echo "Value:" $nameValueArray[1]

           if($nameValuearray[0] -eq "Name"){
            $ParameterObject.ResourceName = $nameValuearray[1]
           }
           $searchKey = "Fn::Define(" + $nameValuearray[0] + ")"
           $objectJsonData = $objectJsonData.Replace($searchKey,$nameValuearray[1])
    }
    $objectJsonData = $objectJsonData + "}"

    $ParameterObject.OutputObject = $objectJsonData|ConvertFrom-Json
}

function ProcessResourcesSection($CloudFormationTemplateObject,$ReturnObject){
    $sectionName = "Resources"
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
   
    $resources = $includedResources|Get-Member|Select-Object Name 


    foreach($resourceName in $resources){

       
        $resourceName = $resourceName.Name
         echo $resourceName
        
        $resourceType = $includedResources.$resourceName."Type"
        echo "AWS Resource Type: $resourceType"
        if($resourceType -eq $null) {
            continue
        }


        $resourceType = GetSimpleTypeFromAWSType($resourceType)
        $templateName = $includedResources.$resourceName."TemplateName"
        
        echo "Simple Type: $resourceType"
        echo "Resource Name: $resourceName"

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceType+ "\"+ $templateName +".template"

        $resource = Get-Content $resourcePath -Raw|ConvertFrom-Json

        $preProcessors = $includedResources.$resourceName."Fn::Define"

        
        $ParameterObject = @{ Object = $resource ; PreProcessors = $preProcessors ; OutputObject =$null;ResourceName = $null }
       
        PreProcessDefines($ParameterObject)
        
        $CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $ParameterObject.ResourceName -NotePropertyValue $ParameterObject.OutputObject
        
       
    }
  
     #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    $sectionObject = $CloudFormationTemplateObject.$sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"
    $sectionObject
   

    #Add the SectionObject back to the main object
    Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject
    $ReturnObject.OutputTemplateObject = $OutputTeamplateObject
   
}

function GenerateTemplate($CloudFormationTemplateObject,$sectionName,$ReturnObject){
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
    $includedResources

    foreach($resourceName in $includedResources){
        echo $resourceName

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceName +".template"

        $resource = Get-Content $resourcePath -Raw|ConvertFrom-Json
        
        $CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $resourceName -NotePropertyValue $resource
       
    }

    $sectionObject = $CloudFormationTemplateObject.$sectionName
    #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"

    $sectionJsonData = $sectionObject|ConvertTo-Json -Depth 1000

    #Add the SectionObject back to the main object
    Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject
    $ReturnObject.OutputTemplateObject = $OutputTeamplateObject
    
 }


$CloudFormationTemplateObject=Get-Content $CloudFormationTemplateSourcePath -Raw|ConvertFrom-Json

$ReturnObject = @{ OutputTemplateObject = $null;JsonData=$null }

GenerateTemplate $CloudFormationTemplateObject "Parameters" $ReturnObject


$CloudFormationTemplateObject = $ReturnObject.OutputTemplateObject

GenerateTemplate $CloudFormationTemplateObject "Mappings" $ReturnObject


$CloudFormationTemplateObject = $ReturnObject.OutputTemplateObject

ProcessResourcesSection $CloudFormationTemplateObject  $ReturnObject

ConvertObjectToJson($ReturnObject) 2>&1> $errorPath 

$ReturnObject.JsonData =  $ReturnObject.JsonData.Replace("   "," ")

$ReturnObject.JsonData | Set-Content $CloudFormationTemplateOutputPath

#$ReturnObject.OutputTemplateObject |ConvertTo-Json -Depth 1000|Set-Content $CloudFormationTemplateOutputPath


 



