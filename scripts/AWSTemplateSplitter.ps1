﻿

function ExtractSection($json,$sectionName){
    echo "Iterating through $sectionName"
    $section = $json."$sectionName"
    $rootFolder = "..\test\$sectionName"

    $subSections = $section| Get-Member|Select-Object Name

    foreach($subSection in $subSections){
        $name = $subSection.Name
        $objType = $section."$name".Type

        if($objType -ne $null -and $sectionName -eq "Resources"){
            $index = $objType.LastIndexOf("::")
            if($index -gt 0 ){
                $index = $index +2;
            }else{
                $index = 0;
            }
            $objType = $objType.Substring($index)
            echo $objType

            $subSectionDirectory = "$rootFolder\$objType"
      
            if(!(Test-Path $subSectionDirectory)  ){
                New-Item -path "$rootFolder" -Name $objType -ItemType directory
            }
            $fileName = $subSectionDirectory + "\" + $name +".template";
          
            $section."$name"|ConvertTo-Json -Depth 1000|Set-Content $fileName
        
        }else{
             if(!(Test-Path $rootFolder)  ){
                New-Item -path "$rootFolder" -ItemType directory
            }
            if(($sectionName -eq "Mappings" -or $sectionName -eq "Parameters") -and !( $name -eq "Equals" -or
                 $name -eq "GetHashCode" -or
                 $name -eq "GetType" -or
                 $name -eq "ToString" ) ){
                $fileName = $rootFolder + "\" + $name +".template";
               
                $section."$name"|ConvertTo-Json -Depth 1000|Set-Content $fileName
            }

        }
     }
 }
$json=Get-Content ..\AWSTemplate_1_DSC.orig.template -Raw|ConvertFrom-Json

 rm -Recurse ..\test
 ExtractSection $json "Parameters"
 ExtractSection $json "Mappings"
 ExtractSection $json "Resources"

 



