﻿function Get-EC2InstanceGuid {
    [CmdletBinding()]
    param(
        $InstanceName
    )

    try {
        Write-Verbose "Downloading CFN Template"
        #$template=Get-Content -RAW .\SingleWebTier_ELB_Name.template|ConvertFrom-Json
        $wc = new-object System.Net.WebClient
      
        $InstanceNameArray = $InstanceName.Split("-")

        if($InstanceNameArray.Count -gt 1){
          $template = $wc.DownloadString('https://s3.amazonaws.com/rakesh-test-resources/stacks_elb/SingleWebTier_ELB_Name.template') | ConvertFrom-Json
               $InstanceName = $InstanceNameArray[1];
               $InstanceKey = $InstanceNameArray[0];
               Write-Verbose "Retrieving instance guid from $InstanceName->$InstanceKey"
               $guid = $template.Mappings."InstancePrefixToGuid"."$InstanceName"."$InstanceKey"
        }else{
           $template = $wc.DownloadString('https://s3.amazonaws.com/quickstart-reference/microsoft/powershelldsc/latest/templates/Template_1_DSC.template') | ConvertFrom-Json
           $guid = $template.Resources.$InstanceName.Properties.Tags.where{$_.key -eq 'guid'}.value
        }
        
        Write-Output $guid
    }
    catch {
         $_ | Write-AWSQuickStartException 
    }
}

Get-EC2InstanceGuid "DC1"