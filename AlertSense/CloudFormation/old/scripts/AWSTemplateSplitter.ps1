﻿$rootFolder = "Z:\runtime\devops\devops\AlertSense\CloudFormation"
$outputFolder = $rootFolder + "\output\CFResources"
$sourceTemplate = $rootFolder + "\AWSTemplate_1_DSC.orig.template"
$errorPath = $rootFolder + "\output\splitter.log"


 rm -Recurse $outputFolder

 New-Item -path "$outputFolder" -ItemType directory

function ConvertObjectToJson($ReturnObject){

    $jsonData = $ReturnObject.IO_Object |ConvertTo-Json -Depth 1000

    $jsonObject = $jsonData |ConvertFrom-Json

    if($jsonObject -eq $null){
        $jsonData = $jsonData  + "\n}"

    }

    $ReturnObject.JsonData = $jsonData.Replace("    ","  ")
    
}
function ExtractSection($json,$sectionName){
    echo "Iterating through $sectionName"
    $section = $json."$sectionName"
    $sectionFolder = $outputFolder + "\" +  $sectionName

    $ReturnObject = @{ IO_Object = $null;OutputJsonData=$null }

    $subSections = $section| Get-Member|Select-Object Name

    foreach($subSection in $subSections){
        $name = $subSection.Name
        $objType = $section."$name".Type

        if($objType -ne $null -and $sectionName -eq "Resources"){
            $index = $objType.LastIndexOf("::")
            if($index -gt 0 ){
                $index = $index +2;
            }else{
                $index = 0;
            }
            $objType = $objType.Substring($index)
            echo $objType

            $subSectionDirectory = "$sectionFolder\$objType"
      
            if(!(Test-Path $subSectionDirectory)  ){
                New-Item -path "$sectionFolder" -Name $objType -ItemType directory
            }
            $fileName = $subSectionDirectory + "\" + $name +".template";
          
            $ReturnObject.IO_Object = $section."$name"
            ConvertObjectToJson($ReturnObject) 2>&1> $errorPath 
            $ReturnObject.JsonData | Set-Content $fileName
            echo "Writing to  $fileName"
           # $section."$name"|ConvertTo-Json -Depth 1000|Set-Content $fileName
        
        }else{
             if(!(Test-Path $sectionFolder)  ){
                New-Item -path "$sectionFolder" -ItemType directory
            }
            if(($sectionName -eq "Mappings" -or $sectionName -eq "Parameters") -and !( $name -eq "Equals" -or
                 $name -eq "GetHashCode" -or
                 $name -eq "GetType" -or
                 $name -eq "ToString" ) ){
                $fileName = $sectionFolder + "\" + $name +".template";
               
                      $ReturnObject.OutputJsonData;
                    $ReturnObject.IO_Object = $section."$name"
                    ConvertObjectToJson($ReturnObject) 2>&1> $errorPath 
                    $ReturnObject.JsonData |Set-Content $fileName
                #$section."$name"|ConvertTo-Json -Depth 1000|Set-Content $fileName
            }

        }
     }
 }
$json=Get-Content $sourceTemplate -Raw|ConvertFrom-Json

 ExtractSection $json "Parameters"
 ExtractSection $json "Mappings"
 ExtractSection $json "Resources"

 



