﻿$RootDirectory = "Z:\runtime\devops\devops\AlertSense\CloudFormation\"
$CFResources = $RootDirectory + "\CFResources"
$SourceDirectory = $RootDirectory + "\scripts"
$OutputDirectory = $RootDirectory + "\output"
$errorPath = $OutputDirectory + "\creator.log"

$CloudFormationTemplateSourcePath = $SourceDirectory+"\WebTier.template"
$CloudFormationTemplateOutputPath = $OutputDirectory+"\WebTier.template"

$ReturnObject = @{ OutputTemplateObject = $null;JsonData=$null }

 rm -Recurse $OutputDirectory 
New-Item -Path "$OutputDirectory" -Name "." -ItemType directory

function GetSimpleTypeFromAWSType($awsResourceType){

        $index = $awsResourceType.LastIndexOf("::")
        if($index -gt 0 ){
            $index = $index +2;
        }else{
            $index = 0;
        }
        $awsResourceType = $awsResourceType.Substring($index)
        return $awsResourceType
    
}

function ConvertObjectToJson($ReturnObject){

    $members = $ReturnObject.OutputTemplateObject | Get-Member -MemberType NoteProperty

    $jsonData = "{"
    foreach($member in $members){
        #Do not append a comma for the first member
        if($jsonData -ne "{"){
            $jsonData = $jsonData + ","
        }

        $memberName = $member.Name
        
        $type = $null
        $type = $ReturnObject.OutputTemplateObject."$memberName"."Type"

        if($memberName -eq "Resources" -or $type -ne $null){
            $save = $ReturnObject.OutputTemplateObject
            $ReturnObject.OutputTemplateObject = $ReturnObject.OutputTemplateObject."$memberName"
            ConvertObjectToJson($ReturnObject)
            $ReturnObject.OutputTemplateObject = $save
            $memberJsonData = $ReturnObject.JsonData
        }else{
            $memberJsonData = $ReturnObject.OutputTemplateObject."$memberName"|ConvertTo-Json -Depth 1000
        }

        $memberObject = $null
         $memberObject = $memberJsonData |ConvertFrom-Json

         if($memberObject -eq $null){
                 $memberJsonData = $memberJsonData  + "}`r`n"

         }

        $jsonData =  $jsonData + """$memberName""" + ":"+ $memberJsonData


        

    }
    $jsonData = $jsonData + "}`r`n"

   # $jsonData = $ReturnObject.IO_Object |ConvertTo-Json -Depth 1000
    $jsonObject = $null
    $jsonObject = $jsonData |ConvertFrom-Json

    if($jsonObject -eq $null){
        $jsonData = $jsonData  + "}`r`n"

    }

    $ReturnObject.JsonData = $jsonData.Replace("    ","  ")
    
}

<#function ConvertObjectToJson($ReturnObject){

    $jsonData = $ReturnObject.OutputTemplateObject |ConvertTo-Json -Depth 1000

    echo "Testing validity of the json data"
    $jsonObject = $jsonData |ConvertFrom-Json  
 

    if($jsonObject -eq $null){
         #Workaround: Add an extra "}" as this seems to be missing while converting to json.
        $jsonData = $jsonData  + "}"
        echo "Found error, corrected"

    }
      
   
    $ReturnObject.JsonData = $jsonData.Replace("    ","  ")
    
}#>
function PreProcessDefines($ParameterObject){
    $preProcessors = $ParameterObject.PreProcessors
    $object = $ParameterObject.Object

    echo "Performing PreProcessing $preProcessors"

    $ReturnObject.OutputTemplateObject = $object

    ConvertObjectToJson($ReturnObject) 2>&1> $errorPath 
  
    #$objectJsonData = $object|ConvertTo-Json -Depth 1000
    $objectJsonData = $ReturnObject.JsonData
    echo "json data 1 $objectJsonData"

    foreach($preProcessor in $preProcessors){
        
           echo "Preprocessor $preProcessor"
           $nameValuearray = $preProcessor.ToString().Split("=")
          
           echo "Name:" $nameValueArray[0]
           echo "Value:" $nameValueArray[1]

           if($nameValuearray[0] -eq "Name"){
            $ParameterObject.ResourceName = $nameValuearray[1]
           }
           $searchKey = "Fn::Define(" + $nameValuearray[0] + ")"
           $objectJsonData = $objectJsonData.Replace($searchKey,$nameValuearray[1])
    }
    #$objectJsonData = $objectJsonData + "\n}"
    
    $ParameterObject.OutputObject = $objectJsonData|ConvertFrom-Json
}

function ProcessResourcesSection($CloudFormationTemplateObject,$ReturnObject){
    $sectionName = "Resources"
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
   
    $resources = $includedResources|Get-Member|Select-Object Name 


    foreach($resourceName in $resources){

       
        $resourceName = $resourceName.Name
         echo $resourceName
        $resourceType = $null
        $resourceType = $includedResources.$resourceName."Type"
        echo "AWS Resource Type: $resourceType"
        if($resourceType -eq $null) {
            continue
        }


        $resourceType = GetSimpleTypeFromAWSType($resourceType)
        $templateName = $includedResources.$resourceName."TemplateName"
        
        echo "Simple Type: $resourceType"
        echo "Resource Name: $resourceName"

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceType+ "\"+ $templateName +".template"

        $resource = Get-Content $resourcePath -Raw|ConvertFrom-Json

        $preProcessors = $includedResources.$resourceName."Fn::Define"

        
        $ParameterObject = @{ Object = $resource ; PreProcessors = $preProcessors ; OutputObject =$null;ResourceName = $null }
       
        PreProcessDefines($ParameterObject)
        
        $CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $ParameterObject.ResourceName -NotePropertyValue $ParameterObject.OutputObject
        
       
    }
  
     #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    $sectionObject = $CloudFormationTemplateObject.$sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"

   
    #Add the SectionObject back to the main object
    Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject
    $ReturnObject.OutputTemplateObject = $OutputTeamplateObject
   
}

function GenerateTemplate($CloudFormationTemplateObject,$sectionName,$ReturnObject){
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
    $includedResources

    foreach($resourceName in $includedResources){
        echo $resourceName

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceName +".template"

        $resource = Get-Content $resourcePath -Raw|ConvertFrom-Json
        
        $CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $resourceName -NotePropertyValue $resource
       
    }

    $sectionObject = $CloudFormationTemplateObject.$sectionName
    #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"

    $sectionJsonData = $sectionObject|ConvertTo-Json -Depth 1000

    #Add the SectionObject back to the main object
    Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject
    $ReturnObject.OutputTemplateObject = $OutputTeamplateObject
    
 }


$CloudFormationTemplateObject=Get-Content $CloudFormationTemplateSourcePath -Raw|ConvertFrom-Json


#Process Parameters section
GenerateTemplate $CloudFormationTemplateObject "Parameters" $ReturnObject
$CloudFormationTemplateObject = $ReturnObject.OutputTemplateObject



#Process mappings section
GenerateTemplate $CloudFormationTemplateObject "Mappings" $ReturnObject
$CloudFormationTemplateObject = $ReturnObject.OutputTemplateObject

# Process the Resources section


ProcessResourcesSection $CloudFormationTemplateObject  $ReturnObject
$CloudFormationTemplateObject = $ReturnObject.OutputTemplateObject



ConvertObjectToJson($ReturnObject) 2>&1> $errorPath 

#$ReturnObject.JsonData =  $ReturnObject.JsonData.Replace("   "," ")

#Workaround: Add an extra "}" as this seems to be missing while converting to json.
#$ReturnObject.JsonData = $ReturnObject.JsonData + "}"
$ReturnObject.JsonData | Set-Content $CloudFormationTemplateOutputPath



#$ReturnObject.OutputTemplateObject |ConvertTo-Json -Depth 1000|Set-Content $CloudFormationTemplateOutputPath


 



