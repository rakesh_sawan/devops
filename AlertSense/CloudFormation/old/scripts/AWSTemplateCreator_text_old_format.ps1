﻿$RootDirectory = "Z:\runtime\devops\devops\AlertSense\CloudFormation\"
$CFResources = $RootDirectory + "\CFResources"
$SourceDirectory = $RootDirectory + "\scripts"
$OutputDirectory = $RootDirectory + "\output"
$errorPath = $OutputDirectory + "\creator.log"

$CloudFormationTemplateSourcePath = $SourceDirectory+"\WebTier.template"
$CloudFormationTemplateOutputPath = $OutputDirectory+"\WebTier.template"

$ReturnObject = @{ OutputTemplateObject = $null;JsonData=$null }

 rm -Recurse $OutputDirectory 
New-Item -Path "$OutputDirectory" -Name "." -ItemType directory

function GetSimpleTypeFromAWSType($awsResourceType){

        $index = $awsResourceType.LastIndexOf("::")
        if($index -gt 0 ){
            $index = $index +2;
        }else{
            $index = 0;
        }
        $awsResourceType = $awsResourceType.Substring($index)
        return $awsResourceType
    
}

function PreProcessDefines($ParameterObject){
    $preProcessors = $ParameterObject.PreProcessors
   

    echo "Performing PreProcessing $preProcessors"

    #$objectJsonData = $object|ConvertTo-Json -Depth 1000
    $objectJsonData = $ParameterObject.JsonData
    echo "json data 1 $objectJsonData"

    foreach($preProcessor in $preProcessors){
        
           echo "Preprocessor $preProcessor"
           $nameValuearray = $preProcessor.ToString().Split("=")
          
           echo "Name:" $nameValueArray[0]
           echo "Value:" $nameValueArray[1]

           if($nameValuearray[0] -eq "Name"){
            $ParameterObject.ResourceName = $nameValuearray[1]
           }
           $searchKey = "Fn::Define(" + $nameValuearray[0] + ")"
           $objectJsonData = $objectJsonData.Replace($searchKey,$nameValuearray[1])
    }
    #$objectJsonData = $objectJsonData + "\n}"
    
    $ParameterObject.JsonData = $objectJsonData
}

function ProcessResourcesSection($CloudFormationTemplateObject,$ReturnObject){
    $sectionName = "Resources"
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
   
    $resources = $includedResources|Get-Member|Select-Object Name 

    $jsonData = "{`r`n"
    foreach($resourceName in $resources){
        
       
        $resourceName = $resourceName.Name
         echo $resourceName
        
        $resourceType = $includedResources.$resourceName."Type"
        echo "AWS Resource Type: $resourceType"
        if($resourceType -eq $null) {
            continue
        }
        if($jsonData -ne "{`r`n" ) {
            $jsonData = $jsonData +",`r`n"
        }

        $resourceType = GetSimpleTypeFromAWSType($resourceType)
        $templateName = $includedResources.$resourceName."TemplateName"
        
        echo "Simple Type: $resourceType"
        echo "Resource Name: $resourceName"

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceType+ "\"+ $templateName +".template"

        $resource = Get-Content $resourcePath -Raw

       

        $preProcessors = $includedResources.$resourceName."Fn::Define"

        
        $ParameterObject = @{ Object = $resource ; PreProcessors = $preProcessors ; OutputObject =$null;ResourceName = $null;JsonData=$resource }
       
        PreProcessDefines($ParameterObject)
         $jsonData = $jsonData + """$resourceName"":" +  $ParameterObject.JsonData
       # $CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $ParameterObject.ResourceName -NotePropertyValue $ParameterObject.OutputObject
        
       
    }
    $jsonData = $jsonData + ","
  
     #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    $sectionObject = $CloudFormationTemplateObject.$sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"

   
    #Add the SectionObject back to the main object
    Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject
    $ReturnObject.JsonData = $jsonData
   
}

function GenerateTemplate($CloudFormationTemplateObject,$sectionName,$ReturnObject){
    echo "Processing  $sectionName section"

    $includedResources = $CloudFormationTemplateObject.$sectionName."Fn::Include"
  
    $jsonData = "{`r`n"

    foreach($resourceName in $includedResources){
        echo $resourceName
        if($jsonData -ne "{" ){
            $jsonData = $jsonData + ",`r`n"
        }

        $resourcePath = $CFResources  + "\" +$sectionName +"\" + $resourceName +".template"
        $resource = Get-Content $resourcePath -Raw

        $jsonData += """$resourceName"": "+$resource
        
        #$CloudFormationTemplateObject."$sectionName" | Add-Member -NotePropertyName $resourceName -NotePropertyValue $resource
       
    }
    $jsonData = $jsonData +"`r`n}"

  <#  $sectionObject = $CloudFormationTemplateObject.$sectionName
    #Get an Object excluding the $sectionName Property
    $OutputTeamplateObject = $CloudFormationTemplateObject|Select-Object -Property * -ExcludeProperty $sectionName

    <# Remove the "Fn::Include" Property from Section Obeject #>
    $sectionObject = $sectionObject|Select-Object -Property * -ExcludeProperty "Fn::Include"

    $sectionJsonData = $sectionObject|ConvertTo-Json -Depth 1000

    #Add the SectionObject back to the main object
   # Add-Member -InputObject $OutputTeamplateObject -NotePropertyName $sectionName -NotePropertyValue $sectionObject#>
    $ReturnObject.JsonData = $jsonData
    
 }


$CloudFormationTemplateObject=Get-Content $CloudFormationTemplateSourcePath -Raw|ConvertFrom-Json


#Process Parameters section

$jsonData ="{`r`n"
GenerateTemplate $CloudFormationTemplateObject "Parameters" $ReturnObject
$jsonData = $jsonData + $ReturnObject.JsonData


#Process mappings section
GenerateTemplate $CloudFormationTemplateObject "Mappings" $ReturnObject
$jsonData = $jsonData + ","+ $ReturnObject.JsonData

# Process the Resources section


ProcessResourcesSection $CloudFormationTemplateObject  $ReturnObject
$jsonData = $jsonData + ","+ $ReturnObject.JsonData



#Workaround: Add an extra "}" as this seems to be missing while converting to json.
$ReturnObject.JsonData = $ReturnObject.JsonData + "`r`n}"
$ReturnObject.JsonData | Set-Content $CloudFormationTemplateOutputPath



#$ReturnObject.OutputTemplateObject |ConvertTo-Json -Depth 1000|Set-Content $CloudFormationTemplateOutputPath


 



